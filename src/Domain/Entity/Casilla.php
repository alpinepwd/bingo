<?php

namespace App\Domain\Entity;

use App\Infrastructure\Doctrine\Repository\CasillaRepository;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping as ORM;


#[Entity(repositoryClass: CasillaRepository::class)]
class Casilla
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: "AUTO")]
    #[ORM\Column(type: "integer")]
    private $id;

    #[ORM\Column(type: "string")]
    private $texto;

    #[ORM\ManyToOne(targetEntity: Jugador::class)]
    private $autor;

    #[ORM\ManyToOne(targetEntity: Jugador::class)]
    private $jugador;

    #[ORM\Column(type: "datetime")]    
    private $dateCreate;

    #[ORM\Column(type: "datetime", nullable: true)]    
    private $dateCheck;

    public function setValues(Jugador $user, Jugador $objetivo, string $prediccion)
    {
        $this->autor = $user;
        $this->jugador = $objetivo;
        $this->texto = $prediccion;
        $this->dateCreate = new \DateTime();
    }

    public function getTexto(){
        return $this->texto;
    }

    public function getId(){
        return $this->id;
    }

    public function getCheck(){
        return null !== $this->dateCheck;
    }

    public function check(){
        $this->dateCheck = new \DateTime();
    }
}
