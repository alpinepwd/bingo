<?php

namespace App\Domain\Manager;

interface ManagerInterface
{
    /**
     * Find one entity by id
     *
     * @param integer $id
     * @return mixed
     */
    public function findOneById(int $id);

    /**
     * Find one entity by the given criteria
     *
     * @param array $criteria
     * @return mixed
     */
    public function findOneBy(array $criteria);

    /**
     * Finds entities by the given criteria
     *
     * @param integer $id
     * @return mixed
     */
    public function findBy(array $criteria): array;

    /**
     * Creates an empty entity instance
     *
     * @return mixed
     */
    public function create();


    /**
     * Saves an entity collection
     *
     * @param array $entities
     * @return void
     */
    public function saveCollection(array $entities): void;

    /**
     * Removes an entity collection
     *
     * @param array $entities
     * @return void
     */
    public function removeCollection(array $entities): void;

    /**
     * Returns all entities
     *
     * @return array
     */
    public function findAll(): array;

    /**
     * Saves an entity
     *
     * @param [type] $entity
     */
    public function save($entity);

    /**
     * Removes an entity
     *
     * @param [type] $entity
     */
    public function remove($entity);
}
