<?php

namespace App\Application\Manager;

use App\Domain\Manager\ManagerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;
use Psr\Log\LoggerInterface;

abstract class AbstractManager implements ManagerInterface
{
    /** @var EntityManagerInterface */
    protected $entityManager;

    /** @var LoggerInterface */
    protected $logger;

    /** @var bool */
    protected $useResultCache = false;

    public function __construct(EntityManagerInterface $entityManager, LoggerInterface $logger)
    {
        $this->entityManager = $entityManager;
        $this->logger = $logger;
    }

    /**
     * @return EntityManagerInterface
     */
    public function getEntityManager(): EntityManagerInterface
    {
        return $this->entityManager;
    }

    public function clearEntityManager(): void
    {
        $this->entityManager->clear();
    }


    /**
     * Get the value of useResultCache
     */
    public function isUseResultCache()
    {
        return $this->useResultCache;
    }

    /**
     * Set the value of useResultCache
     *
     * @return  self
     */
    public function setUseResultCache($useResultCache)
    {
        $this->useResultCache = $useResultCache;

        return $this;
    }

    protected function getRepository(string $class): ObjectRepository
    {
        return $this->getEntityManager()->getRepository($class);
    }

    /**
     * Saves an entity
     *
     * @param $entity
     */
    public function save($entity)
    {
        $this->entityManager->persist($entity);
        $this->entityManager->flush();
    }

    public function remove($entity)
    {
        $this->entityManager->remove($entity);
        $this->entityManager->flush();
    }
}
