<?php


namespace App\Application\Manager;

use App\Domain\Entity\Casilla;
use App\Domain\Entity\Jugador;
use App\Infrastructure\Doctrine\Repository\CasillaRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class CasillaManager extends AbstractManager
{
    /** @var CasillaRepository */
    private $repository;

    function __construct(EntityManagerInterface $entityManager, LoggerInterface $logger)
    {
        parent::__construct($entityManager, $logger);
        $this->repository = $this->getRepository(Casilla::class);
    }

    /**
     * Find one entity by id
     *
     * @param integer $id
     * @return Casilla|null
     */
    public function findOneById(int $id)
    {

        return $this->repository->find($id);
    }

    /**
     * Find one entity by the given criteria
     *
     * @param array $criteria
     * @return Casilla|null
     */
    public function findOneBy(array $criteria)
    {
        return $this->repository->findOneBy($criteria);
    }

    /**
     * Finds entities by the given criteria
     *
     * @param integer $id
     * @return Casilla[]
     */
    public function findBy(array $criteria): array
    {
        return $this->repository->findBy($criteria);
    }

    /**
     * Creates an empty entity instance
     *
     * @return Casilla
     */
    public function create()
    {
        return new Casilla();
    }


    /**
     * Saves an entity collection
     *
     * @param Casilla[] $casillas
     * @return void
     */
    public function saveCollection(array $casillas): void
    {
        foreach ($casillas as $casilla) {
            $this->entityManager->persist($casilla);
        }
        $this->entityManager->flush();
    }

    /**
     * Removes an entity collection
     *
     * @param Casilla[] $casillas
     * @return void
     */
    public function removeCollection(array $casillas): void
    {
        foreach ($casillas as $casilla) {
            $this->entityManager->persist($casilla);
        }
        $this->entityManager->flush();
    }

    /**
     * Retrieves all casilla
     *
     * @return array|Casilla[]
     */
    public function findAll(): array
    {
        return $this->repository->findAll();
    }

}
