<?php


namespace App\Application\Manager;

use App\Domain\Entity\Jugador;
use App\Infrastructure\Doctrine\Repository\JugadorRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class JugadorManager extends AbstractManager
{
    /** @var JugadorRepository */
    private $repository;

    function __construct(EntityManagerInterface $entityManager, LoggerInterface $logger)
    {
        parent::__construct($entityManager, $logger);
        $this->repository = $this->getRepository(Jugador::class);
    }

    /**
     * Find one entity by id
     *
     * @param integer $id
     * @return Jugador|null
     */
    public function findOneById(int $id)
    {

        return $this->repository->find($id);
    }

    /**
     * Find one entity by the given criteria
     *
     * @param array $criteria
     * @return Jugador|null
     */
    public function findOneBy(array $criteria)
    {
        return $this->repository->findOneBy($criteria);
    }

    /**
     * Finds entities by the given criteria
     *
     * @param integer $id
     * @return Jugador[]
     */
    public function findBy(array $criteria): array
    {
        return $this->repository->findBy($criteria);
    }

    /**
     * Creates an empty entity instance
     *
     * @return Jugador
     */
    public function create()
    {
        return new Jugador();
    }


    /**
     * Saves an entity collection
     *
     * @param Jugador[] $jugadors
     * @return void
     */
    public function saveCollection(array $jugadors): void
    {
        foreach ($jugadors as $jugador) {
            $this->entityManager->persist($jugador);
        }
        $this->entityManager->flush();
    }

    /**
     * Removes an entity collection
     *
     * @param Jugador[] $jugadors
     * @return void
     */
    public function removeCollection(array $jugadors): void
    {
        foreach ($jugadors as $jugador) {
            $this->entityManager->persist($jugador);
        }
        $this->entityManager->flush();
    }

    /**
     * Retrieves all jugador
     *
     * @return array|Jugador[]
     */
    public function findAll(): array
    {
        return $this->repository->findAll();
    }

    public function obtenerDemasJugadores(Jugador $jugador){
        return $this->repository->obtenerDemasJugadores($jugador);
    }
}
