<?php

namespace App\Application\DTO;

class BingoDTO
{
    /** @var int */
    public $jugador;

    /** @var PrediccionDTO[] */
    public $predicciones;
}
