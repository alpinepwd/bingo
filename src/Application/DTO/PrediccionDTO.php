<?php

namespace App\Application\DTO;

class PrediccionDTO
{
    /** @var int */
    public $id;

    /** @var string */
    public $texto;

    /** @var bool */
    public $check;
}
