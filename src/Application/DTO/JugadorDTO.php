<?php

namespace App\Application\DTO;

class JugadorDTO
{
    /** @var int */
    public $id;

    /** @var string */
    public $nombre;
}
