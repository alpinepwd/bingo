<?php

namespace App\Application\Service;

use App\Application\DTO\JugadorDTO;
use App\Application\Manager\JugadorManager;
use App\Domain\Entity\Jugador;

class JugadoresService
{
    /** @var JugadorManager */
    private $jugadorManager;

    function __construct(JugadorManager $jugadorManager)
    {
        $this->jugadorManager = $jugadorManager;
    }

    /**
     * Undocumented function
     *
     * @param \App\Domain\Entity\Jugador $jugador
     * @return JugadorDTO[]
     */
    public function restoJugadores(Jugador $jugador)
    {
        $lista = [];
        $restoRaw = $this->jugadorManager->obtenerDemasJugadores($jugador);
        foreach ($restoRaw as $value) {
            $dto = new JugadorDTO();
            $dto->nombre = $value['j_nombre'];
            $dto->id = $value['j_id'];

            $lista[]= $dto;
        }

        return $lista;
    }
}
