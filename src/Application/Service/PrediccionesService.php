<?php

namespace App\Application\Service;

use App\Application\DTO\BingoDTO;
use App\Application\DTO\PrediccionDTO;
use App\Application\Manager\CasillaManager;
use App\Application\Manager\JugadorManager;
use App\Domain\Entity\Casilla;
use App\Domain\Entity\Jugador;

class PrediccionesService
{

    /** @var CasillaManager */
    private $casillaManager;

    /** @var JugadorManager */
    private $jugadorManager;

    function __construct(CasillaManager $casillaManager, JugadorManager $jugadorManager)
    {
        $this->casillaManager = $casillaManager;
        $this->jugadorManager = $jugadorManager;
    }


    public function addPrediccionDeJugador(Jugador $user, array $contenido)
    {
        $jugadorObjetivo = $this->jugadorManager->findOneById($contenido['jugador']);


        $prediccion = $contenido['prediccion'];
        $casilla = new Casilla();
        $casilla->setValues($user, $jugadorObjetivo, $prediccion);
        $this->casillaManager->save($casilla);
    }

    public function obtenerPrediccionesExcepto(Jugador $user){
        $demasJugadores = $this->jugadorManager->obtenerDemasJugadores($user);
        $bingos = [];
        foreach($demasJugadores as $jugador){
            $bingo = new BingoDTO();
            $bingo->jugador = $jugador['j_nombre'];
            $bingo->predicciones = [];
            $casillas = $this->casillaManager->findBy(['jugador' => $jugador['j_id']]);
            foreach($casillas as $casilla){
                $prediccion = new PrediccionDTO();
                $prediccion->id = $casilla->getId();
                $prediccion->texto = $casilla->getTexto();
                $prediccion->check = $casilla->getCheck();
                $bingo->predicciones[] = $prediccion; 
            }

            $bingos[] = $bingo;
        }
        $bingo = new BingoDTO();
        $bingo->jugador = "Mías";
        $casillas = $this->casillaManager->findBy(['jugador' => $user]);
        foreach($casillas as $casilla){
            if(!$casilla->getCheck()){
                continue;
            }
            $prediccion = new PrediccionDTO();
            $prediccion->id = $casilla->getId();
            $prediccion->texto = $casilla->getTexto();
            $prediccion->check = $casilla->getCheck();
            $bingo->predicciones[] = $prediccion; 
        }
        $bingos[] = $bingo;

        return $bingos;
    }

    public function check( $id){
        $prediccion = $this->casillaManager->findOneById($id);
        $prediccion->check(new \DateTime());
        $this->casillaManager->save($prediccion);
    }
}
