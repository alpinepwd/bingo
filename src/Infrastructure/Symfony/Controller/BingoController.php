<?php

namespace App\Infrastructure\Symfony\Controller;

use App\Application\Service\JugadoresService;
use App\Application\Service\PrediccionesService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

#[Route(name: 'bingo_')]
class BingoController extends AbstractController
{
    #[Route('/', name: 'main')]
    public function index(AuthenticationUtils $authenticationUtils): Response
    {
        $this->denyAccessUnlessGranted('ROLE_PLAYER');
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUserName = $authenticationUtils->getLastUsername();

        return $this->render(
            'bingo/index.html.twig',
            [
                'last_username' => $lastUserName,
                'error' => $error,
            ]
        );
    }

    #[Route('/add', name: 'view_add')]
    public function add(JugadoresService $jugadoresService): Response
    {
        $this->denyAccessUnlessGranted('ROLE_PLAYER');
        $jugador = $this->getUser();
        $prediccionesDelJugador = [];
        $jugadores = $jugadoresService->restoJugadores($jugador);
        return $this->render(
            'bingo/add.html.twig',
            [
                'jugadores' => $jugadores,
                'predicciones' => []
            ]
        );
    }

    #[Route('/show', name: 'view_show')]
    public function show(PrediccionesService $prediccionesService): Response
    {
        $this->denyAccessUnlessGranted('ROLE_PLAYER');

        $user = $this->getUser();

        $prediccionesExceptoJugador = $prediccionesService->obtenerPrediccionesExcepto($user);

        return $this->render(
            'bingo/show.html.twig',
            [
                'predicciones' => $prediccionesExceptoJugador,
            ]
        );
    }

    #[Route('/add-prediction', name: "add_prediction", methods: ["POST"])]
    public function addPrediccion(Request $request, PrediccionesService $prediccionesService)
    {
        $this->denyAccessUnlessGranted('ROLE_PLAYER');

        $user = $this->getUser();
        $contenido = json_decode($request->getContent(), true);

        $prediccionesService->addPrediccionDeJugador($user, $contenido);

        return new Response();
    }

    #[Route('/check', name: "check_prediction", methods: ["PUT"])]
    public function check(Request $request, PrediccionesService $prediccionesService)
    {
        $this->denyAccessUnlessGranted('ROLE_PLAYER');

        $user = $this->getUser();
        $id = $request->getContent();

        $prediccionesService->check($id);

        return new Response();
    }
}
