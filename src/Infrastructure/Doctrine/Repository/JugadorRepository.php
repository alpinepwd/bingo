<?php

namespace App\Infrastructure\Doctrine\Repository;

use App\Domain\Entity\Jugador;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Laminas\Code\Generator\DocBlock\Tag\ReturnTag;

class JugadorRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Jugador::class);
    }

    public function obtenerDemasJugadores(Jugador $jugador){
        $qb = $this->createQueryBuilder('j')
        ->select(['j.id as j_id', 'j.nombre as j_nombre'])
        ->where('j.id != :jugador_id')
        ->setParameter('jugador_id', $jugador);

        return $qb->getQuery()->getArrayResult();
    }
}
