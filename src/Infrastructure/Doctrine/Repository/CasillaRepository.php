<?php

namespace App\Infrastructure\Doctrine\Repository;

use App\Domain\Entity\Casilla;
use App\Domain\Entity\Jugador;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class CasillaRepository extends ServiceEntityRepository
{
 public function __construct(ManagerRegistry $registry)
 {
     parent::__construct($registry, Casilla::class);
 }

}